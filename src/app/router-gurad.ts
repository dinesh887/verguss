import {CanActivate, Router} from '@angular/router';
import {Injectable} from '@angular/core';
import {DataCacheHandlerService} from './data-cache/data-cache-handler.service';

@Injectable()
export class RouterGurad implements CanActivate {

  constructor(protected router: Router, private dataCacheHandler: DataCacheHandlerService) {
  }

  canActivate() {
    if (this.dataCacheHandler.getDataStore('users')) {
      // logged in so return true
      return true;
    }
    // not logged in so redirect to login page
    this.router.navigate(['/']);
    return false;
  }
}
