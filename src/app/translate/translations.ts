import { InjectionToken } from '@angular/core';
import { LangEnTranslations } from './lang-en';
import { LangDeTranslations } from './lang-de';
export const Translations: any = new InjectionToken('translations');

// All translations
const dictionary = {
  'en': LangEnTranslations,
  'de': LangDeTranslations
};

export const TranslationProviders = [
  { provide: Translations, useValue: dictionary }
];
