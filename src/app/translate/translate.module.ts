import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslatePipe } from './translate.pipe';
import {TranslateService} from './translate.service';
import {TranslationProviders} from './translations';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [TranslatePipe],
  exports: [TranslatePipe]
})
export class TranslateModule {
  static forRoot() {
    return {
      ngModule: TranslateModule,
      providers: [TranslateService, TranslationProviders]
    };
  }
}
