import {EventEmitter, Inject, Injectable} from '@angular/core';
import {Translations} from './translations';

@Injectable({
  providedIn: 'root'
})
export class TranslateService {
  private currentLang: string;
  languageChanged: EventEmitter<boolean> = new EventEmitter<boolean>();
  constructor(@Inject(Translations) private translations: any) { }
  public getCurrentLang() {
    return this.currentLang;
  }

  public setCurrentLang(lang: string): void {
    this.currentLang = lang;

    if (this.currentLang === 'de') {
      this.languageChanged.emit(true);
    } else {
      this.languageChanged.emit(false);
    }
  }

  public instant(key: string) {
    return this.translate(key);
  }

  public isEnglish(): boolean {
    return this.currentLang === 'en';
  }

  public translate(key: string, lang?: string): string {
    let translation = key;
    let translateLang = this.currentLang;

    if (lang) {
      translateLang = lang;
    }

    if (this.translations[translateLang] && this.translations[translateLang][key]) {
      return this.translations[translateLang][key];
    }
    return translation;
  }
}
