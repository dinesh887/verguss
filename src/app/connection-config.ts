import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {  of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';


export class AppConfig {

  readonly wsUrl: string;
  readonly baseUrl: string;
  readonly apiUrl: string;
  readonly assetUrl: string;

}
export let CONNECTION_CONFIG: AppConfig;
@Injectable()
export class ConnectionConfig {
  private appConfig;

  constructor(private http: HttpClient) {}

  public load() {
    return new Promise((resolve, reject) => {
      this.http.get('assets/configurations/connection.json').pipe(map(res => res as any),
        catchError(error => of('Server error')))
        .subscribe((envResponse: any) => {
          const t = new AppConfig();
          CONNECTION_CONFIG = Object.assign(t, envResponse);
          resolve(true);
        });

    });
  }
}
