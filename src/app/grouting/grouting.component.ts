import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { RecipeDialogComponent } from './recipe-dialog/recipe-dialog.component';
import { GroutingEvents } from './grouting-events';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { LocalService } from '../modules/local-service/local.service';
import { MessageService } from '../message/message.service';
import { MotorMessage } from '../modules/local-service/motors/motor-message';
import { AppConfig } from '../conf/config';

@Component({
  selector: 'app-grouting',
  templateUrl: './grouting.component.html',
  styleUrls: [ './grouting.component.css' ]
})
export class GroutingComponent implements OnInit, OnChanges {
  @Input() selectedItem: any;
  selectedOrder: any = {};
  selectedRecipe: any = {};
  selectedRecipeName: string = '';
  selectedRecipeGrouting: string = '';
  recipeGroutingList: any[] = [];
  profileOpelForm: FormGroup;
  recipesList: any;
  outerwidth: number = 0;
  innerwidth: number = 0;
  height: number = 0;
  allCommandList: any = {};

  constructor(public dialog: MatDialog,
              public groutingEvent: GroutingEvents,
              public fb: FormBuilder,
              private localService: LocalService,
              private messageService: MessageService
  ) {
    this.groutingEvent.selectRecipeEvent.subscribe(data => {
      this.setSelectedRecipe(data);
    });
  }

  ngOnInit() {
    this.profileOpelForm = this.fb.group({
      fillheight: new FormControl('', Validators.required),
      recipeGrouting: new FormControl('', Validators.required),
    });
    this.localService.getCommands().subscribe(data => {
      this.allCommandList = data;
    });
  }

  ngOnChanges() {
    this.selectedOrder = this.selectedItem;
  }

  openRecipeList() {
    const that = this;
    this.localService.getRecipes(1).subscribe(data => {
      try {
        that.recipesList = data;
        const resData = [];
        for (const key in data) {
          if (data.hasOwnProperty(key)) {
            resData.push(key);
          }
        }
        this.dialog.open(RecipeDialogComponent, {
          width: '500px',
          data: {
            recipes: resData
          }
        });
      } catch (e) {
        console.log(e);
      }
    });

  }

  setSelectedRecipe(recipe: any) {
    this.selectedRecipe = this.recipesList[ recipe ];
    this.selectedRecipeName = recipe;
    this.selectedRecipeGrouting = this.selectedRecipe.recipeGrouting;
    this.outerwidth = this.selectedRecipe.outerwidth;
    this.innerwidth = this.selectedRecipe.innerwidth;
    this.height = this.selectedRecipe.height;
    this.profileOpelForm.controls[ 'fillheight' ].setValue(this.selectedRecipe.fillheight);
    this.localService.getRecipes(2).subscribe(data => {
      console.log(data);
      for (const key in data) {
        if (data.hasOwnProperty(key)) {
          data[ key ][ 'key' ] = key;
          this.recipeGroutingList.push(data[ key ]);

        }
      }
    });
    this.profileOpelForm.controls[ 'recipeGrouting' ].setValue(this.selectedRecipe.recipeGrouting);
  }

  saveClick() {
    try {
      if (this.getFormRecipe()[ 'outerwidth' ]) {
        this.recipesList[ this.selectedRecipeName ] = this.getFormRecipe();
        this.updateRecipe(this.recipesList);
      } else {
        // TODO disable save,delete button
      }
    } catch (e) {
      this.messageService.error('Error in json  !');
    }
  }

  updateRecipe(recipes: any) {
    try {
      if (this.getFormRecipe()[ 'outerwidth' ]) {
        this.localService.updateRecipes(1, JSON.stringify(recipes, undefined, 4)).subscribe(data => {
          if (data.code === 1) {
            this.messageService.success('Success !');
          }
        });
      } else {
        // TODO disable save,delete button
      }
    } catch (e) {
      this.messageService.error('Error in json  !');
    }
  }

  getFormRecipe() {
    const recipe = {};
    recipe[ 'outerwidth' ] = +this.outerwidth;
    recipe[ 'innerwidth' ] = +this.innerwidth;
    recipe[ 'height' ] = +this.height;
    recipe[ 'fillheight' ] = +this.profileOpelForm.controls[ 'fillheight' ].value;
    recipe[ 'recipeGrouting' ] = this.profileOpelForm.controls[ 'recipeGrouting' ].value;
    recipe[ 'profiltyp' ] = this.selectedRecipe.profiltyp;
    recipe[ 'comment' ] = this.selectedRecipe.comment;
    return recipe;
  }

  deleteClick() {
    delete this.recipesList[ this.selectedRecipeName ];
    console.log(this.recipesList);
    this.updateRecipe(this.recipesList);
    this.clearRecipeForm();
  }

  clearRecipeForm() {
    this.profileOpelForm.controls[ 'outerwidth' ].setValue('');
    this.profileOpelForm.controls[ 'innerwidth' ].setValue('');
    this.profileOpelForm.controls[ 'height' ].setValue('');
    this.profileOpelForm.controls[ 'fillheight' ].setValue('');
    this.profileOpelForm.controls[ 'recipeGrouting' ].setValue('');
  }

  clickControlButton(button: any) {
    if (AppConfig.selectedMotorPort) {
      const clickedCommand = this.allCommandList.filter(item => item.key === button);
      let motorMessage = new MotorMessage();
      motorMessage = clickedCommand;
      motorMessage.port = AppConfig.selectedMotorPort;
      this.localService.sendMessage(motorMessage).subscribe(result => {
        if (result) {
        }
      }, error => {
        this.messageService.error('Server Error !');
      });
    } else {
      this.messageService.error('Please Select a port');
    }
  }
}
