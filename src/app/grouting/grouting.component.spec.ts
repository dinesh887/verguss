import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GroutingComponent } from './grouting.component';

describe('GroutingComponent', () => {
  let component: GroutingComponent;
  let fixture: ComponentFixture<GroutingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GroutingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroutingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
