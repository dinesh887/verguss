import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { GroutingEvents } from '../grouting-events';

@Component({
  selector: 'app-recipe-dialog',
  templateUrl: './recipe-dialog.component.html',
  styleUrls: [ './recipe-dialog.component.css' ]
})
export class RecipeDialogComponent implements OnInit {
  recipes: any[] = [];

  constructor(private dialogRef: MatDialogRef<RecipeDialogComponent>,
              private selectRecipeEvent: GroutingEvents,
              @Inject(MAT_DIALOG_DATA) public data: any) {
  }

  ngOnInit() {
    this.recipes = this.data.recipes;
  }

  close() {
    this.dialogRef.close();
  }

  clickOnRecipe(recipe: any) {
    // this.selectedRecipe = this.recipesList[ recipe ];
    this.selectRecipeEvent.selectRecipeEvent.emit(recipe);
    this.dialogRef.close();
  }
}
