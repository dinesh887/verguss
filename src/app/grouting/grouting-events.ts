import { EventEmitter, Injectable } from '@angular/core';

@Injectable()
export class GroutingEvents {
  selectRecipeEvent: EventEmitter<any> = new EventEmitter();
}
