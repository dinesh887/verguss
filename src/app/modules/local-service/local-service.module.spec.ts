import { LocalServiceModule } from './local-service.module';

describe('LocalServiceModule', () => {
  let localServiceModule: LocalServiceModule;

  beforeEach(() => {
    localServiceModule = new LocalServiceModule();
  });

  it('should create an instance', () => {
    expect(localServiceModule).toBeTruthy();
  });
});
