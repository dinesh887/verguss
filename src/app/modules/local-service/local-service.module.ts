import {NgModule} from '@angular/core';
import {MotorsComponent} from './motors/motors.component';
import {LocalService} from './local.service';
import {SharedModule} from '../shared/shared.module';
import { IosComponent } from './ios/ios.component';
import {VergussDataTableComponent} from '../../verguss-data-table/verguss-data-table.component';
import {IosGirdComponent} from './ios/ios.gird.component';
import { RecipesComponent } from './recipes/recipes.component';

@NgModule({
  imports: [
    SharedModule
  ],
  declarations: [
    MotorsComponent,
    IosComponent,
    IosGirdComponent,
    RecipesComponent
  ],
  providers: [
    LocalService
  ]
})
export class LocalServiceModule {
}
