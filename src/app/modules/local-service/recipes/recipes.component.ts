import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { LocalService } from '../local.service';
import { MessageService } from '../../../message/message.service';
import { TextBox } from '../../verguss-dynamic-form/text-box';
import { FormGroups } from '../../verguss-dynamic-form/form-groups';
import { AppConfig } from '../../../conf/config';

@Component({
  selector: 'app-recipes',
  templateUrl: './recipes.component.html',
  styleUrls: [ './recipes.component.css' ],
})
export class RecipesComponent implements OnInit {
  recipeForm: FormGroup;
  formFields: any[] = [];
  formFieldsRecipeVR: any[] = [];
  updateEndPoint: string;
  updateVrEndPoint: string;

  constructor(private fb: FormBuilder,
              private localService: LocalService,
              private messageService: MessageService) {
  }

  ngOnInit() {
    this.recipeForm = this.fb.group({
      recipePro: new FormControl('', Validators.required),
      recipeVr: new FormControl('', Validators.required)
    });
    this.getRecipes();
    this.getRecipesVr();
    this.updateEndPoint = AppConfig.api.baseUrl + 'updateRecipes';
    this.updateVrEndPoint = AppConfig.api.baseUrl + 'updateRecipesVr';
  }

  getRecipes() {
    this.localService.getRecipes(1).subscribe(data => {
      try {
        const groupArr = [];
        const recipes = JSON.stringify(data, undefined, 4);
        this.recipeForm.controls[ 'recipePro' ].setValue(recipes);
        for (const key in data) {
          if (data.hasOwnProperty(key)) {
            const formGroup = new FormGroups();
            formGroup.groupLable = key;
            const fieldArray = [];
            for (const keyFeild in data[ key ]) {
              if (data[ key ].hasOwnProperty(keyFeild)) {
                const textBox = new TextBox(
                  {
                    key: key + '_' + keyFeild,
                    label: keyFeild,
                    value: data[ key ][ keyFeild ],
                    type: 'text',
                    order: 2
                  }
                );
                fieldArray.push(textBox);
              }
            }
            formGroup.formFields = fieldArray;
            groupArr.push(formGroup);

          }

        }
        this.formFields = groupArr;
      } catch (e) {
        console.log(e);
      }

    });
  }

  getRecipesVr() {
    this.localService.getRecipes(2).subscribe(data => {
      try {
        const groupArr = [];
        const recipes = JSON.stringify(data, undefined, 4);
        this.recipeForm.controls[ 'recipeVr' ].setValue(recipes);
        for (const key in data) {
          if (data.hasOwnProperty(key)) {
            const formGroup = new FormGroups();
            formGroup.groupLable = key;
            const fieldArray = [];
            for (const keyFeild in data[ key ]) {
              if (data[ key ].hasOwnProperty(keyFeild)) {
                const textBox = new TextBox(
                  {
                    key: key + '_' + keyFeild,
                    label: keyFeild,
                    value: data[ key ][ keyFeild ],
                    type: 'text',
                    order: 2
                  }
                );
                fieldArray.push(textBox);
              }
            }
            formGroup.formFields = fieldArray;
            groupArr.push(formGroup);

          }

        }
        this.formFieldsRecipeVR = groupArr;
      } catch (e) {
        console.log(e);
      }
    });
  }

  // updateRecipe(type) {
  //   try {
  //     let jsonData = '';
  //     if (type === 1) {
  //       jsonData = this.recipeForm.controls[ 'recipePro' ].value;
  //     } else {
  //       jsonData = this.recipeForm.controls[ 'recipeVr' ].value;
  //     }
  //     if (this.localService.isJson(jsonData)) {
  //       this.localService.updateRecipes(type, jsonData).subscribe(data => {
  //         if (data.code === 1) {
  //           this.messageService.success('Success !');
  //         }
  //       });
  //     } else {
  //       this.messageService.error('JSON format is incorrect !');
  //     }
  //   } catch (e) {
  //     console.log(e);
  //   }
  // }
}
