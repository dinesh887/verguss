import {Component, OnInit} from '@angular/core';
import {WebSocketService} from '../../../communication/web-socket.service';
import {VergussDataTableComponent} from '../../../verguss-data-table/verguss-data-table.component';
import {LocalService} from '../local.service';
import {MessageService} from '../../../message/message.service';

@Component({
  selector: 'app-ios',
  templateUrl: './ios.component.html'
})
export class IosComponent  implements OnInit {
  dataset: any[] = [];
  columnList: any = [];
  lastUpdated: Date;
  moxaMessage: any;

  constructor(private webSocketService: WebSocketService,
              private localService: LocalService,
              private messageService: MessageService) {
    this.webSocketService.socket.emit('subscribeForMoxa');
  }

  ngOnInit() {
    this.webSocketService.socket.on('moxaUpdates', data => {
      this.dataset = data;
      this.moxaMessage = data;
      // this.setDataSource(this.dataset);
      this.lastUpdated = new Date();
    });
  //  this.setDisplayedColums(this.getDisplyColumn());
    // this.setColumnDataMap(this.getDataColumnMapping());
  }

  // getDisplyColumn(): any {
  //   const arr = [];
  //   arr.push('relayIndex');
  //   arr.push('relayMode');
  //   arr.push('relayStatus');
  //   arr.push('relayTotalCount');
  //   arr.push('relayCurrentCount');
  //   arr.push('relayCurrentCountReset');
  //   arr.push('#');
  //
  //   return arr;
  // }
  //
  // getDataColumnMapping(): any {
  //   const arr = [];
  //   arr.push({key: 'relayIndex', mappingColumn: 'relayIndex', showIcon: false});
  //   arr.push({key: 'relayMode', mappingColumn: 'relayMode', showIcon: false});
  //   arr.push({key: 'relayStatus', mappingColumn: 'relayStatus', showIcon: false});
  //   arr.push({key: 'relayTotalCount', mappingColumn: 'relayTotalCount', showIcon: false});
  //   arr.push({key: 'relayCurrentCount', mappingColumn: 'relayCurrentCount', showIcon: false});
  //   arr.push({key: 'relayCurrentCountReset', mappingColumn: 'relayCurrentCountReset', showIcon: false});
  //   arr.push({
  //     key: '#', mappingColumn: '#',
  //     showIcon: false,
  //     showButton: true,
  //     buttonText: 'ON'
  //   });
  //   return arr;
  // }
  //
  // setButtonText(row: any, col: any) {
  //   if (col) {
  //     if (row.relayStatus === 1) {
  //       col.buttonText = 'OFF';
  //     } else {
  //       col.buttonText = 'ON';
  //     }
  //   } else {
  //     const buttonColumn = this.columnDataMap.find(item => item.showButton === true);
  //     if (row.relayStatus === 1) {
  //       buttonColumn.buttonText = 'OFF';
  //     } else {
  //       buttonColumn.buttonText = 'ON';
  //     }
  //   }
  // }

  // actionClick(row: any) {
  //   try {
  //     if (this.moxaMessage) {
  //       for (let i = 0; i < this.moxaMessage.io.relay.length; i++) {
  //         if (row.relayIndex === this.moxaMessage.io.relay[i].relayIndex) {
  //           this.moxaMessage.io.relay[i].relayStatus = this.moxaMessage.io.relay[i].relayStatus === 0 ? 1 : 0;
  //         }
  //       }
  //       // row.relayStatus = !row.relayStatus ? 1 : 0;
  //       this.localService.setMoxaStatus(this.moxaMessage).subscribe(data => {
  //         this.setButtonText(row, undefined);
  //       }, error => {
  //         this.messageService.error('Error !');
  //       });
  //     }
  //   } catch (e) {
  //     console.log(e);
  //   }
  // }
}
