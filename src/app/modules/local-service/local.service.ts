import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {MotorMessage} from './motors/motor-message';
import {Observable} from 'rxjs/index';
import {HttpConnection} from '../../communication/http-connection';
import { AppConfig } from '../../conf/config';

@Injectable({
  providedIn: 'root'
})
export class LocalService {
  // baseUrl = AppConfig.api.baseUrl;
  // assetUrl = AppConfig.api.assetUrl;

  constructor(private httpConnection: HttpConnection) {
  }

  getPortList(): Observable<any> {
    const url = AppConfig.api.baseUrl + 'getPortList';
    return this.httpConnection.get(url);
    // const url = 'http://localhost:4200/assets/commands/testPorts.json';
    // return this.httpConnection.get(url);
  }

  sendMessage(motorMessage: MotorMessage): Observable<any> {
    const url = AppConfig.api.baseUrl + 'sendMessage';
    return this.httpConnection.post(motorMessage, url);
  }

  getCommands(): Observable<any> {
    const url = AppConfig.api.assetUrl + 'commands/commands.json';
    return this.httpConnection.get(url);
  }

  getStatus(motorMessage: MotorMessage): Observable<any> {
    const url = AppConfig.api.baseUrl + 'getStatus?port=' + motorMessage.port;
    return this.httpConnection.get(url);
  }

  setMoxaStatus(message: any): Observable<any> {
    const url = AppConfig.api.baseUrl + 'setIos';
    return this.httpConnection.post(message, url);
  }

  getRecipes(file: any): Observable<any> {
    let url = '';
    if (file === 1) {
      url = AppConfig.api.baseUrl + 'getRecipes';
    } else {
      url = AppConfig.api.baseUrl + 'getRecipeVr';
    }
    return this.httpConnection.get(url);
  }

  updateRecipes(type: any, json: any): Observable<any> {
    const url = AppConfig.api.baseUrl + 'updateRecipes';
    const data = {};
    data['fileType'] = type;
    data['requestPrams'] = json;
    return this.httpConnection.post(data, url);
  }

  isJson(str: string): boolean {
    try {
      const result = JSON.parse(str);
      if (result === null) {
        return false;
      }

    } catch (e) {
      return false;
    }
    return true;
  }
}
