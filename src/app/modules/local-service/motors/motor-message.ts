export class MotorMessage {
  address: number = 1;
  command: number = 0;
  type: number = 0;
  motor: number = 0;
  value: number = 0;
  name: string;
  port: string;
  key: number;
}
