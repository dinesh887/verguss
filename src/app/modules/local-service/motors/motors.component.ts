import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MotorMessage } from './motor-message';
import { MessageService } from '../../../message/message.service';
import { LocalService } from '../local.service';
import { AppConfig } from '../../../conf/config';

@Component({
  selector: 'app-motors',
  templateUrl: './motors.component.html',
  styleUrls: [ './motors.component.css' ]
})
export class MotorsComponent implements OnInit {
  portList: any[] = [];
  actionList: any[] = [];
  selectPort: any;
  selectAction: any;
  motorStatus: any[] = [];
  mortorForm: FormGroup;
  disableButton: boolean = false;
  allCommandList: any = {};

  constructor(private localService: LocalService,
              private fb: FormBuilder,
              private messageService: MessageService) {
  }

  ngOnInit() {
    this.mortorForm = this.fb.group({
      selectPort: new FormControl('', Validators.required),
      selectAction: new FormControl('', Validators.required)
    });
    this.localService.getPortList().subscribe(data => {
      if (data) {
        this.portList = data.portList;
        this.disableButton = false;
      } else {
        this.disableButton = true;
      }

    }, error => {
      setTimeout(() => this.messageService.error('Server is not running '));
      this.disableButton = true;
    });
    this.localService.getCommands().subscribe(data => {
      this.actionList = data.filter(item => item.showInDropdown === true);
      this.allCommandList = data;
    });
  }

  clickSend() {
    let motorMessage = new MotorMessage();
    if (!this.mortorForm.valid) {
      this.messageService.error('Both fields are required ');
      return;
    }
    motorMessage = this.mortorForm.controls[ 'selectAction' ].value;
    motorMessage.port = this.mortorForm.controls[ 'selectPort' ].value.comName;
    AppConfig.selectedMotorPort = motorMessage.port;
    this.localService.sendMessage(motorMessage).subscribe(result => {
      if (result) {
        this.getStatusData(result.data);
      }
    }, error => {
      this.messageService.error('Server Error !');
    });
  }

  changePort() {
    AppConfig.selectedMotorPort = this.mortorForm.controls[ 'selectPort' ].value.comName;
  }

  clickControlButton(button: any) {
    const clickedCommand = this.allCommandList.filter(item => item.key === button);
    let motorMessage = new MotorMessage();
    if (!this.mortorForm.controls[ 'selectPort' ].value.comName) {
      this.messageService.error('Please select port ');
      return;
    }
    motorMessage = clickedCommand;
    motorMessage.port = this.mortorForm.controls[ 'selectPort' ].value.comName;
    this.localService.sendMessage(motorMessage).subscribe(result => {
      if (result) {
        this.getStatusData(result.data);
      }
    }, error => {
      this.messageService.error('Server Error !');
    });
  }

  getStatusData(data: any) {
    this.motorStatus[ 'value' ] = (data[ 4 ] << 24) | (data[ 5 ] << 16) | (data[ 6 ] << 8) | data[ 7 ];
    this.motorStatus[ 'status' ] = data[ 2 ] === 100 ? 'OK' : 'Failed';
    this.motorStatus[ 'address' ] = data[ 0 ];
  }

}
