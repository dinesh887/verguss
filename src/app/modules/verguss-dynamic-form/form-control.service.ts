import { Injectable } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { FormGroups } from './form-groups';
import { HttpConnection } from '../../communication/http-connection';
import { MessageService } from '../../message/message.service';
import { Observable } from 'rxjs';
import { CommonRequest } from './common-request';

@Injectable()
export class FormControlService {

  constructor(private httpConnection: HttpConnection, private messageService: MessageService) {
  }

  toFormGroup(formBase: FormGroups[]) {
    const group: any = {};

    formBase.forEach(form => {
      form.formFields.forEach(fr => {
        group[ fr.key ] = fr.required ? new FormControl(fr.value || '', Validators.required)
          : new FormControl(fr.value || '');
      });

    });
    return new FormGroup(group);
  }

  updateData(data: any, endPoint: string): Observable<any> {
     const request = new CommonRequest();
     request.requestPrams = data;
    return this.httpConnection.post(request, endPoint);
  }
}
