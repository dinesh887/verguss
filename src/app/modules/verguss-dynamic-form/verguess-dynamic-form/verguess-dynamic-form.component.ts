import {Component, Input, OnInit} from '@angular/core';
import {FormBase} from '../form-base';
import {FormGroup} from '@angular/forms';

@Component({
  selector: 'app-verguess-dynamic-form',
  templateUrl: './verguess-dynamic-form.component.html',
  styleUrls: ['./verguess-dynamic-form.component.css']
})
export class VerguessDynamicFormComponent {

  @Input() formBase: FormBase<any>;
  @Input() form: FormGroup;

  get isValid() {
   return this.form.controls[this.formBase.key].valid;
  }

}
