import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerguessDynamicFormComponent } from './verguess-dynamic-form.component';

describe('VerguessDynamicFormComponent', () => {
  let component: VerguessDynamicFormComponent;
  let fixture: ComponentFixture<VerguessDynamicFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerguessDynamicFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerguessDynamicFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
