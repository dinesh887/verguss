import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VerguessDynamicFormComponent } from './verguess-dynamic-form/verguess-dynamic-form.component';
import { DynamicFormComponent } from './dynamic-form/dynamic-form.component';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule, MatCardModule, MatListModule } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    BrowserModule, ReactiveFormsModule,
    MatCardModule,
    MatListModule,
    MatButtonModule
  ],
  declarations: [ VerguessDynamicFormComponent, DynamicFormComponent ],
  exports: [ DynamicFormComponent ]
})
export class VergussDynamicFormModule {
}
