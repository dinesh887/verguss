import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { FormBase } from '../form-base';
import { FormGroup } from '@angular/forms';
import { FormControlService } from '../form-control.service';
import { FormGroups } from '../form-groups';
import { MessageService } from '../../../message/message.service';

@Component({
  selector: 'app-dynamic-form',
  templateUrl: './dynamic-form.component.html',
  styleUrls: [ './dynamic-form.component.css' ],
  providers: [ FormControlService ]
})
export class DynamicFormComponent implements OnInit, OnChanges {

  @Input() fromBase: FormBase<any>[] = [];
  @Input() formGroups: FormGroups[] = [];
  @Input() endPoint: string;
  form: FormGroup;
  changeSet = '';

  constructor(private fcs: FormControlService, private messageService: MessageService) {
  }

  ngOnInit() {
  }

  ngOnChanges() {
    this.form = this.fcs.toFormGroup(this.formGroups);
  }

  onSubmit() {
    const data = this.form.value;
    const temObj = {};
    for (const key in data) {
      if (data.hasOwnProperty(key)) {
        const keySplit = key.split('_');
        if (!temObj.hasOwnProperty(keySplit[ 0 ])) {
          temObj[ keySplit[ 0 ] ] = {};
          temObj[ keySplit[ 0 ] ][ keySplit[ 1 ] ] = data[ key ];
        } else {
          temObj[ keySplit[ 0 ] ][ keySplit[ 1 ] ] = data[ key ];
        }


      }

    }
    this.changeSet = JSON.stringify(temObj, undefined, 4);

    this.fcs.updateData(this.changeSet, this.endPoint).subscribe(res => {
      this.messageService.success('Successfully Updated');
    }, error => {
      this.messageService.error('Internal Error !');
    });
    console.log(this.endPoint);
  }

}
