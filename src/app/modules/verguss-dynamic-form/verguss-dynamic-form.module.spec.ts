import { VergussDynamicFormModule } from './verguss-dynamic-form.module';

describe('VergussDynamicFormModule', () => {
  let vergussDynamicFormModule: VergussDynamicFormModule;

  beforeEach(() => {
    vergussDynamicFormModule = new VergussDynamicFormModule();
  });

  it('should create an instance', () => {
    expect(vergussDynamicFormModule).toBeTruthy();
  });
});
