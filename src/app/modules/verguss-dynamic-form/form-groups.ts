import { FormBase } from './form-base';

export class FormGroups {
  groupId: string;
  groupLable: string;
  formFields: FormBase<any>[] = [];
}
