export const AppConfig = {
  api: {
    'baseUrl': 'http://localhost:3000/',
    'assetUrl': 'http://localhost:3000/assets/',
    'apiUrl': 'http://192.168.10.195/smartProduction/',
    'wsUrl': 'http://localhost:3000/'
  },
  selectedMotorPort: undefined
};
