import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {OrderService} from '../order-list/order.service';
import {MessageService} from '../message/message.service';
import {DataCacheHandlerService} from '../data-cache/data-cache-handler.service';
import {MatDialog} from '@angular/material';
import {ManualScanDialogComponent} from './manual-scan-dialog/manual-scan-dialog.component';
import {ManualScanEvents} from './manual-scan-events';

@Component({
  selector: 'app-manual-scan',
  templateUrl: './manual-scan.component.html',
  styleUrls: ['./manual-scan.component.css']
})
export class ManualScanComponent implements OnInit {
  orderForm: FormGroup;
  externalDataSource: any = [];
  externalDataSourceByUser: any = [];
  columnList: any = [];
  columnListNameOrders: any = [];
  userList: any;
  stationsId: any;
  status: any;
  orderStatusIcon: string = undefined;
  disableButtone: boolean = true;
  selectedName: any;

  constructor(private orderService: OrderService,
              private fb: FormBuilder,
              private messageService: MessageService,
              private dataCache: DataCacheHandlerService,
              public dialog: MatDialog,
              public manulaScanEvent: ManualScanEvents) {
    this.manulaScanEvent.createNewEvent.subscribe(data => {
      this.orderForm.controls['name'].setValue(data.selectedName);
      this.changeState(true);
    });
  }

  ngOnInit() {
    this.orderForm = this.fb.group({
      orderNumber: new FormControl('', Validators.required),
      name: new FormControl('', Validators.required)
    });
    this.columnList = {
      dispalyedColumns: this.getDisplyColumn(),
      columnMapping: this.getDataColumnMapping()
    };
    this.columnListNameOrders = {
      dispalyedColumns: this.getDisplyColumNameOrder(),
      columnMapping: this.getDataColumnMappingNameOrder()
    };
    this.userList = this.dataCache.getDataStore('users').value ? this.dataCache.getDataStore('users').value.all : [];
    this.stationsId = this.dataCache.getDataStore('stationsId') ? this.dataCache.getDataStore('stationsId').value : 0;
    this.status = this.dataCache.getDataStore('status') ? this.dataCache.getDataStore('status').value : {};
  }

  pressEnterOnOrderNumber(event): void {
    if (event.keyCode === 13) {
      this.updateOrders();
    }
  }

  getStatusIcon(order: any) {
    const status = (order[0].oTcpState == 4) ? order[0].oTcpState : order[0].tcpState;
    let icon = '';
    switch (status) {
      case 0:
        icon = 'fa fa-times icon-border stop-icon-color ';
        break;
      case 1:
        icon = 'fa fa-pause inprogress-icon-color';
        break;
      case 2:
        icon = 'fa fa-play inprogress-icon-color';
        break;
      case 3:
        icon = 'fa fa-square done-icon-color';
        break;
      case 4:
        icon = 'fa fa-square stop-icon-color';
        break;
      case 5:
        icon = 'fa fa-square-o';
        break;
      case 6:
        icon = 'fa fa-male icon-border';
        break;
      case 99:
        icon = 'fa fa-minus';
        break;
    }
    this.orderStatusIcon = icon;
  }

  clickChangeStatus(finish) {
    this.changeState(finish);
  }

  pressEnterOnName(event, finish): void {
    if (event.keyCode === 13) {
      this.changeState(finish);
    }
  }

  updateOrders() {
    try {
      const orderNumber = this.orderForm.controls['orderNumber'].value;

      this.orderService.getStationByOrderNumber(this.stationsId, orderNumber).subscribe((data: any) => {
        this.externalDataSource = data;
        if (data.length > 0 || data[0].oTcpState == this.status.INIT || data[0].oTcpState == this.status.STOPPED) {
          this.disableButtone = false;
        }
        this.getStatusIcon(data);
      }, (error) => {
        if (error.responseText === '') {
          this.messageService.error('Auftrag ist nicht vorhanden!');
        } else {
          this.messageService.error(error.responseText);
        }
      });
    } catch (e) {
      this.messageService.error('Internal Error');
      console.log(e);
    }
  }

  changeState(finish) {
    try {
      const orderNumber = this.orderForm.controls['orderNumber'].value;
      let name = this.orderForm.controls['name'].value;
      if (name === undefined || name === '') {
        this.messageService.error('Benutzer eintragen!');
        return;
      } else if (!(/\s/g.test(name))) {
        const nameObj = this.userList.find(x => x.AD === name.toUpperCase());
        if (typeof nameObj !== 'undefined') {
          name = nameObj.name;
        } else {
          this.messageService.error('Benutzer existiert nicht!');
          return;
        }
      }
      // Not approved orders can not be started;
      if (this.externalDataSource[0].tcpState === this.status.INIT) {
        this.messageService.error('Auftrag ist noch nicht freigegeben');
        return;
      }
      this.orderService.updateStationByName(this.stationsId, orderNumber, name, finish).subscribe((data: any) => {
        this.externalDataSourceByUser = data;
        this.selectedName = name;
        this.orderForm.controls['name'].setValue('');
        this.updateOrders();
      }, (error) => {
        if (error.responseText === '') {
          this.messageService.error('Auftrag ist nicht vorhanden!');
        } else {
          this.messageService.error(error.responseText);
        }
      });
    } catch (e) {
      console.log(e);
      this.messageService.error('Internal Error');
    }
  }

  getDisplyColumn(): any {
    const arr = [];
    arr.push('ORDERNUMBER');
    arr.push('STARTDATE');
    arr.push('ENDDATE');
    arr.push('NAME');
    return arr;
  }

  getDataColumnMapping(): any {
    const arr = [];
    arr.push({key: 'ORDERNUMBER', mappingColumn: 'orderNumber', showIcon: false});
    arr.push({key: 'STARTDATE', mappingColumn: 'startdate', showIcon: false});
    arr.push({key: 'ENDDATE', mappingColumn: 'enddate', showIcon: false});
    arr.push({key: 'NAME', mappingColumn: 'name', showIcon: false});
    return arr;
  }

  getDisplyColumNameOrder(): any {
    const arr = [];
    arr.push('ORDERNUMBER');
    arr.push('STATION');
    arr.push('STARTDATE');
    arr.push('ENDDATE');
    arr.push('NAME');
    return arr;
  }

  getDataColumnMappingNameOrder(): any {
    const arr = [];
    arr.push({key: 'ORDERNUMBER', mappingColumn: 'orderNumber', showIcon: false});
    arr.push({key: 'STATION', mappingColumn: 'id', showIcon: false});
    arr.push({key: 'STARTDATE', mappingColumn: 'startdate', showIcon: false});
    arr.push({key: 'ENDDATE', mappingColumn: 'enddate', showIcon: false});
    arr.push({key: 'NAME', mappingColumn: 'name', showIcon: false});
    return arr;
  }

  clickStationFinished() {
    this.dialog.open(ManualScanDialogComponent, {
      width: '500px',
      data: {orderNumber: this.orderForm.controls['orderNumber'].value}
    });
  }

  changeName() {
    this.selectedName = this.orderForm.controls['name'].value;
  }

}
