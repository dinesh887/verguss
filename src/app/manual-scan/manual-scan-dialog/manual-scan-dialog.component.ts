import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MatDialogRef} from '@angular/material';
import {MessageService} from '../../message/message.service';
import {DataCacheHandlerService} from '../../data-cache/data-cache-handler.service';
import {ManualScanEvents} from '../manual-scan-events';

@Component({
  selector: 'app-manual-scan-dialog',
  templateUrl: './manual-scan-dialog.component.html',
  styleUrls: ['./manual-scan-dialog.component.css']
})
export class ManualScanDialogComponent implements OnInit {
  newOrder: FormGroup;

  constructor(private dialogRef: MatDialogRef<ManualScanDialogComponent>,
              private fb: FormBuilder,
              private messageService: MessageService,
              private dataCache: DataCacheHandlerService,
              private manualScanEvents: ManualScanEvents) {
  }

  ngOnInit() {
    this.newOrder = this.fb.group({
      name: new FormControl('', Validators.required)
    });
  }

  close() {
    this.dialogRef.close();
  }

  save() {
    if (this.newOrder.controls['name'].value) {
      this.manualScanEvents.createNewEvent.emit({selectedName: this.newOrder.controls['name'].value});
      this.dialogRef.close();
    } else {
      this.messageService.error('Benutzer eintragen!');
      return;
    }
  }
}
