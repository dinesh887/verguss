import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManualScanDialogComponent } from './manual-scan-dialog.component';

describe('ManualScanDialogComponent', () => {
  let component: ManualScanDialogComponent;
  let fixture: ComponentFixture<ManualScanDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManualScanDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManualScanDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
