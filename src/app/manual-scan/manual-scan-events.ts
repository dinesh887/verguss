import {EventEmitter, Injectable} from '@angular/core';

@Injectable()
export class ManualScanEvents {
  createNewEvent: EventEmitter<any> = new EventEmitter();
}
