import {Injectable} from '@angular/core';
import {DataCacheService} from './data-cache.service';
import { CacheStatus, DataCacheEvents } from './data-cache-events';
import { CONNECTION_CONFIG } from '../connection-config';
import { AppConfig } from '../conf/config';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class DataCacheHandlerService {
  private dataStore: {
    dataCache: DataCache[]
  };
  // private dataCacheStatusList: Map<string, CacheStatus> = new Map();

  constructor(private dataCacheService: DataCacheService, private  dataCacheEvents: DataCacheEvents) {
    this.dataStore = {dataCache: []};
  }

  initCache() {
    AppConfig.api['baseUrl'] = CONNECTION_CONFIG.baseUrl;
    AppConfig.api['assetUrl'] = CONNECTION_CONFIG.assetUrl;
    AppConfig.api['apiUrl'] = CONNECTION_CONFIG.apiUrl;
    AppConfig.api['wsUrl'] = CONNECTION_CONFIG.wsUrl;
    this.dataCacheService.getUserList().subscribe(userList => {
      this.setDataStore('users', userList);
      this.dataCacheEvents.dataCacheInitialize.emit(100);
    });
    this.dataCacheService.getAppConfig().subscribe(config => {
      this.setDataStore('status', config.states);
      this.setDataStore('stations', config.stations);
      this.setDataStore('stationsId', config.id);
    });
  }

  setDataStore(id: string, data: any) {
    const dataCache = new DataCache();
    dataCache.id = id;
    dataCache.value = data;
    const store = this.getDataStore(id);
    if (store) {
      this.removeFromDataStore(id);
      this.dataStore.dataCache.push(dataCache);
    } else {
      this.dataStore.dataCache.push(dataCache);
    }
  }

  getDataStore(id: string): DataCache {
    let store: DataCache = new DataCache();
    this.dataStore.dataCache.forEach((item, index) => {
      if (item.id === id) {
        store = this.dataStore.dataCache[index];
      }
    });
    return store;
  }

  removeFromDataStore(id: string) {
    this.dataStore.dataCache.forEach((item, index) => {
      if (item.id === id) {
        this.dataStore.dataCache.slice(index, 1);
      }
    });
  }

  // getDataCacheInitializedPercentage() {
  //   let total = 0;
  //   let finished = 0;
  //   let percentage = 0;
  //   this.dataCacheStatusList.forEach(item => {
  //     total = total + item.total;
  //     finished = finished + item.finished;
  //   });
  //   if (total > 0) {
  //     percentage = (finished / total) * 100;
  //   }
  //   return percentage;
  // }
}

export class DataCache {
  id: string;
  value: any;
}
