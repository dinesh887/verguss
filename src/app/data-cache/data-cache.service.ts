import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpConnection} from '../communication/http-connection';
import {Observable} from 'rxjs/index';
import { AppConfig } from '../conf/config';

@Injectable({
  providedIn: 'root'
})
export class DataCacheService {
  // apiUrl = AppConfig.api.apiUrl;

  constructor(private httpConnection: HttpConnection) {
  }

  getUserList(): Observable<any> {
    const url = AppConfig.api.apiUrl + '/Tools/api/Config/getUsers.php';
    const data = '';
    return this.httpConnection.postAsFormData(data, url);
    // const url = 'http://localhost:4200/assets/commands/testUsers.json';
    // return this.httpConnection.get(url);
  }

  getAppConfig(): Observable<any> {
    const url = AppConfig.api.apiUrl + '/Tools/api/Config/GetConfig.php';
    const data = 'StationName=Profilcenter';
    return this.httpConnection.postAsFormData(data, url);
    // const url = 'http://localhost:4200/assets/commands/testConfig.json';
    // return this.httpConnection.get(url);
  }
}
