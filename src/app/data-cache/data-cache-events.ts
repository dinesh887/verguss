import {EventEmitter, Injectable} from '@angular/core';

@Injectable()
export class DataCacheEvents {
  dataCacheInitialize: EventEmitter<number> = new EventEmitter();
}
export class CacheStatus {
  total: number;
  finished: number;

  constructor(total: number, finished: number) {
    this.total = total;
    this.finished = finished;
  }
}
