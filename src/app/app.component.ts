import { Component, Injector } from '@angular/core';
import { TranslateService } from './translate/translate.service';
import { DataCacheHandlerService } from './data-cache/data-cache-handler.service';
import { Router } from '@angular/router';
import { WebSocketService } from './communication/web-socket.service';
import { DataCacheEvents } from './data-cache/data-cache-events';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: [ './app.component.css' ]
})
export class AppComponent {
  private injector: Injector;
  showSpinner: boolean = true;
  cacheInitilizedPercentage: number = 0;
  cacheInitilizedCircleDispalyDashArray: number = 339.292;
  cacheInitilizedCircleDispalyDashOffset: number = 339.292;
  color = 'primary';
  mode = 'indeterminate';
  value = 50;
  constructor(private translate: TranslateService,
              private dataCacheHandlerService: DataCacheHandlerService,
              private dataCacheEvents: DataCacheEvents,
              private router: Router, private webSocket: WebSocketService) {
    this.translate.setCurrentLang('en');
    this.dataCacheHandlerService.initCache();
    this.dataCacheEvents.dataCacheInitialize.subscribe(data => {
      if (data === 100) {
        this.showSpinner = false;
        this.router.navigate([ 'orders' ]);
      }
    });
    this.webSocket.socket.on('connected', function (data) {

    });

  }
}
