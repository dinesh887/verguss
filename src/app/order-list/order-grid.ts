import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {VergussDataTableComponent} from '../verguss-data-table/verguss-data-table.component';
import {DataTableEvent} from '../verguss-data-table/data-table-event';
import {OrderService} from './order.service';

@Component({
  selector: 'app-order-grid',
  templateUrl: '../verguss-data-table/verguss-data-table.component.html'
})
export class OrderGrid extends VergussDataTableComponent implements OnInit, OnChanges {
  @Input() mode: any;
  @Input() inputDataSource: any;

  constructor(public dataTableEvent: DataTableEvent, private orderService: OrderService) {
    super(dataTableEvent);
  }

  ngOnInit() {
    if (this.mode == 2) {
      this.setDisplayedColums(this.getDisplyColumnOpen());
      this.setColumnDataMap(this.getDataColumnMappingOpen());
      this.setTableClass('table-height-800 table-scroll');
    } else {
      this.setDisplayedColums(this.getDisplyColumn());
      this.setColumnDataMap(this.getDataColumnMapping());
      this.setTableClass('table-height-520 table-scroll');
    }

  }

  ngOnChanges() {
    this.setDataSource(this.inputDataSource);
  }

  getDataColumnMappingOpen(): any {
    const arr = [];
    arr.push({key: 'ORDERNUMBER', mappingColumn: 'orderNumber', showIcon: false});
    arr.push({key: 'ESTIMATEDSTART', mappingColumn: 'estimatedStart', showIcon: false});
    arr.push({key: 'ESTIMATEDEND', mappingColumn: 'estimatedEnd', showIcon: false});
    arr.push({key: 'DELIVERY', mappingColumn: 'deliver', showIcon: false});
    arr.push({
      key: 'ORDERSTATE',
      mappingColumn: 'oTcpState',
      showIcon: true
    });
    arr.push({
      key: 'STATIONSTATE', mappingColumn: 'tcpState',
      showIcon: true
    });
    arr.push({key: 'ASSIGNEDPERSON', mappingColumn: 'name', showIcon: false});
    arr.push({
      key: '#', mappingColumn: '#',
      showIcon: false,
      showButton: true,
      buttonText: 'offen'
    });
    return arr;
  }

  getDisplyColumnOpen(): any {
    const arr = [];
    arr.push('ORDERNUMBER');
    arr.push('ESTIMATEDSTART');
    arr.push('ESTIMATEDEND');
    arr.push('DELIVERY');
    arr.push('ORDERSTATE');
    arr.push('STATIONSTATE');
    arr.push('ASSIGNEDPERSON');
    arr.push('#');
    return arr;
  }

  isDisabled(data: any) {
    if ([0, 1, 4, 5].indexOf(data['oTcpState']) > -1) {
      return true;
    }
  }

  setIcons(selectedItem: any) {
    if (this.mode == 2) {
      return this.setIconOpenOrderList(selectedItem);
    } else {
      return this.setIconOrderList(selectedItem);
    }

  }

  getDataColumnMapping(): any {
    const arr = [];
    arr.push({key: 'AMOUNT', mappingColumn: 'Z_Anzahl', showIcon: false});
    arr.push({key: 'ORDERNUMBER', mappingColumn: 'DokumentNrAUF', showIcon: false});
    arr.push({key: 'PARTNUMBER', mappingColumn: 'ArtikelNrLAG', showIcon: false});
    arr.push({key: 'TYPE', mappingColumn: 'Z_Profiltyp', showIcon: false});
    arr.push({
      key: 'TRACKING',
      mappingColumn: 'tcpState',
      showIcon: true
    });
    arr.push({key: 'POSITION', mappingColumn: 'Z_Position', showIcon: false});
    arr.push({key: 'LENGTH', mappingColumn: 'Z_Profill_nge_L1_mm', showIcon: false});
    arr.push({key: 'CAP', mappingColumn: 'Z_Breite_Enddeckel__mm', showIcon: false});
    arr.push({key: 'GROUTING', mappingColumn: 'Z_Uin__V', showIcon: false});
    arr.push({key: 'POWER', mappingColumn: 'Z_Leistung___LED_Angaben', showIcon: false});
    arr.push({key: 'COLORTEMP', mappingColumn: 'Z_Farbtemperatur___CRI', showIcon: false});
    arr.push({key: 'PROFILTYPE', mappingColumn: 'Z_Profiltyp_1', showIcon: false});
    arr.push({key: 'CABLEOUTLET', mappingColumn: 'Z_Typ_Kabelabgang', showIcon: false});
    return arr;
  }

  getDisplyColumn(): any {
    const arr = [];
    arr.push('AMOUNT');
    arr.push('ORDERNUMBER');
    arr.push('PARTNUMBER');
    arr.push('TYPE');
    arr.push('TRACKING');
    arr.push('POSITION');
    arr.push('LENGTH');
    arr.push('CAP');
    arr.push('GROUTING');
    arr.push('POWER');
    arr.push('COLORTEMP');
    arr.push('PROFILTYPE');
    arr.push('CABLEOUTLET');
    return arr;
  }

  setIconOpenOrderList(selectedItem: any) {
    let icon = '';
    switch (selectedItem['oTcpState']) {
      case 0:
        icon = 'fa fa-times icon-border stop-icon-color';
        break;
      case 1:
        icon = 'fa fa-pause inprogress-icon-color';
        break;
      case 2:
        icon = 'fa fa-play inprogress-icon-color';
        break;
      case 3:
        icon = 'fa fa-square done-icon-color';
        break;
      case 4:
        icon = 'fa fa-square stop-icon-color';
        break;
      case 5:
        icon = 'fa fa-square-o';
        break;
      case 6:
        icon = 'fa fa-male icon-border';
        break;
      case 99:
        icon = 'fa fa-minus';
        break;
    }
    return icon;
  }

  setIconOrderList(selectedItem: any) {
    let icon = '';
    switch (selectedItem['tcpState']) {
      case 0:
        icon = 'fa fa-times icon-border stop-icon-color';
        break;
      case 1:
        icon = 'fa fa-pause inprogress-icon-color';
        break;
      case 2:
        icon = 'fa fa-play inprogress-icon-color';
        break;
      case 3:
        icon = 'fa fa-square done-icon-color';
        break;
      case 4:
        icon = 'fa fa-square stop-icon-color';
        break;
      case 5:
        icon = 'fa fa-square-o';
        break;
      case 6:
        icon = 'fa fa-male icon-border';
        break;
      case 99:
        icon = 'fa fa-minus';
        break;
    }
    return icon;
  }

  setColors(selectedItem: any) {
    if (this.mode !== 2) {
      let color = '';
      if (selectedItem['Z_Skizze'] === 1) {
        color = 'red';
      } else if (selectedItem['tcpState'] === 3) {
        color = '#93C11C';
      } else if (selectedItem['tcpState'] === 4) {
        color = '#575953';
      } else if (selectedItem['tcpState'] === 0) {
        color = '#575953';
      }
      return color;
    }
  }

  actionClick(selectedRow: any) {
    this.dataTableEvent.actionButtonClickEvent.emit(selectedRow);
  }
}
