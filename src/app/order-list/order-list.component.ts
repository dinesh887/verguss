import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { OrderService } from './order.service';
import { DataTableEvent } from '../verguss-data-table/data-table-event';
import { MessageService } from '../message/message.service';
import { DataCacheHandlerService } from '../data-cache/data-cache-handler.service';


@Component({
  selector: 'app-order-list',
  templateUrl: './order-list.component.html',
  styleUrls: [ './order-list.component.css' ]
})
export class OrderListComponent implements OnInit {
  orderForm: FormGroup;
  externalDataSource: any[] = [];
  orderListMode: any;
  isOpenOrder: boolean = false;
  userList: any[] = [];
  stationsId: any;
  selectedOrder: any = {};

  constructor(private orderService: OrderService,
              private fb: FormBuilder,
              private route: ActivatedRoute,
              private dataTableEvent: DataTableEvent,
              private router: Router,
              private messageService: MessageService,
              private dataCache: DataCacheHandlerService) {
    this.dataTableEvent.actionButtonClickEvent.subscribe(data => {
      this.router.navigate([ '/orders/orderNumber/', data.orderNumber ]);
    });
    this.dataTableEvent.rowClickEvent.subscribe(data => {
      this.selectedOrder = data;
    });
  }

  ngOnInit() {
    this.genarateView();
    try {
      this.userList = this.dataCache.getDataStore('users').value.all;
      this.stationsId = this.dataCache.getDataStore('stationsId') ? this.dataCache.getDataStore('stationsId').value : 20;
      if (this.route.snapshot.params[ 'id' ] == 2) {
        this.isOpenOrder = true;
        this.orderListMode = 2;
        this.getOpenOrders();
      } else {
        if (this.route.snapshot.params[ 'orderNumber' ]) {
          this.orderService.getOrders(this.stationsId,
            this.route.snapshot.params[ 'orderNumber' ]).subscribe((data: any) => {
            this.externalDataSource = data.orderObj;
          });
        }
      }
    } catch (e) {
      this.router.navigate(['/']);
      // setTimeout(() =>  this.messageService.error('Can not find users'));
    }
  }

  pressEnter(event): void {
    if (event.keyCode === 13) {
      const orderNumber = this.orderForm.controls[ 'orderNumber' ].value;
      if (!orderNumber) {
        this.messageService.error('Auftrag wurde schon eingelesen!');
      }
      this.orderService.getOrders(this.stationsId, orderNumber).subscribe((data: any) => {
        this.externalDataSource = data.orderObj;
      });
    }
  }

  genarateView(){
    this.orderForm = this.fb.group({
      orderNumber: new FormControl('', Validators.required),
      user: new FormControl('', Validators.required)
    });
  }
  getOpenOrders() {
    this.orderService.getOpenOrders(20).subscribe((data: any) => {
      this.externalDataSource = data;
    });
  }

  stopOrder() {
    const selectedUser = this.orderForm.controls[ 'user' ].value;
    if (!selectedUser) {
      this.messageService.error('Bitte Name eintragen!');
      return;
    }

  }
}
