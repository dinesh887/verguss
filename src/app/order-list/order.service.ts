import { Injectable } from '@angular/core';
import { HttpConnection } from '../communication/http-connection';
import { Observable } from 'rxjs/index';
import { AppConfig } from '../conf/config';

@Injectable({
  providedIn: 'root'
})
export class OrderService {
  // apiUrl = AppConfig.api.apiUrl;

  constructor(private httpConnection: HttpConnection) {
  }

  getOrders(stationId: any, orderId: any): Observable<any> {
    const url = AppConfig.api.apiUrl + '/Tools/api/Database/getOrders.php';
    const data = 'OrderNumber=' + orderId + '&StationId=' + stationId;
    return this.httpConnection.postAsFormData(data, url);
    // const url = 'http://localhost:4200/assets/commands/testOrders.json';
    // return this.httpConnection.get(url);
  }

  getStationByOrderNumber(stationId: any, orderId: any): Observable<any> {
    const url = AppConfig.api.apiUrl + '/Tools/api/Database/getStationObjects.php';
    const data = 'OrderNumber=' + orderId + '&StationId=' + stationId;
    return this.httpConnection.postAsFormData(data, url);
    // const url = 'http://localhost:4200/assets/commands/testStation.json';
    // return this.httpConnection.get(url);
  }

  updateStationByName(stationId: any, orderId: any, user: any, finish: boolean): Observable<any> {
    const url = AppConfig.api.apiUrl + '/Tools/api/Database/updateStation.php';
    const data = 'OrderNumber=' + orderId + '&Station=' + stationId + '&Name=' + user + '&Finish=' + finish;
    return this.httpConnection.postAsFormData(data, url);
    // const url = 'http://localhost:4200/assets/commands/testOrderByname.json';
    // return this.httpConnection.get(url);
  }

  getOpenOrders(stationId: any): Observable<any> {
    const url = AppConfig.api.apiUrl + '/Tools/api/Database/getOrders.php';
    const data = 'StationId=' + stationId;
    return this.httpConnection.postAsFormData(data, url);
    // const url = 'http://localhost:4200/assets/commands/testOpenOrders.json';
    // return this.httpConnection.get(url);
  }


}
