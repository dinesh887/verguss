import {Injectable} from '@angular/core';
import {ToastrService} from 'ngx-toastr';


@Injectable({
  providedIn: 'root'
})
export class MessageService {
  messageFadeoutTime = 10000;
  title = 'Verguss';
  constructor(private toastr: ToastrService) {
  }
  info(summary: string, detail?: string) {
    this.toastr.info(summary, this.title);
  }
  error(summary: string, detail?: string) {
    this.toastr.error(summary, this.title);
  }
  success(summary: string, detail?: string) {
    this.toastr.success(summary, this.title);
  }
  warn(summary: string, detail?: string) {
    this.toastr.warning(summary, this.title);
  }
}
