import {Injectable} from '@angular/core';
import * as io from 'socket.io-client';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class WebSocketService {
  socket: SocketIOClient.Socket;
  wsUrl = environment.wsUrl;
  constructor() {
    this.socket = io.connect(this.wsUrl);
  }
}
