import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Router} from '@angular/router';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/index';

@Injectable()
export class HttpConnection {
  headers: HttpHeaders;
  headersFromData: HttpHeaders;

  constructor(public http: HttpClient, private router: Router) {
    this.headers = new HttpHeaders().set('Content-Type', 'application/json');
    this.headersFromData = new HttpHeaders().set( 'Content-Type', 'application/x-www-form-urlencoded;charset=utf-8;');
  }

  public post(data: any, path: any): Observable<any> {
    return this.http
      .post(path, JSON.stringify(data), {headers: this.headers});
  }
  public get(path): Observable<any> {
    return this.http
      .get<any[]>(path, {headers: this.headers});
  }
  public postAsFormData(data: any, path: any): Observable<any> {
    return this.http
      .post(path, data, {headers: this.headersFromData});
  }
}
