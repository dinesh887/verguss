import {Component} from '@angular/core';
import {BreakpointObserver, Breakpoints, BreakpointState} from '@angular/cdk/layout';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {TranslateService} from '../translate/translate.service';

@Component({
  selector: 'main-nav',
  templateUrl: './main-nav.component.html',
  styleUrls: ['./main-nav.component.css']
})
export class MainNavComponent {
  private lang = 'de';
  isCollapsed = true;
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );
  constructor(private breakpointObserver: BreakpointObserver,   private translateService: TranslateService) {
  }
  public changeLanguage() {
    if (this.lang === 'en') {
      this.lang = 'de';
    } else {
      this.lang = 'en';
    }
    this.translateService.setCurrentLang(this.lang);
  }

}
