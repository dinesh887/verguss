import {EventEmitter, Injectable} from '@angular/core';

@Injectable()
export class DataTableEvent {
  actionButtonClickEvent: EventEmitter<any> = new EventEmitter();
  rowClickEvent: EventEmitter<any> = new EventEmitter();
}
