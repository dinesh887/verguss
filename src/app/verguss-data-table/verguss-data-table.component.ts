import { Component, Input, OnChanges, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { VergussDataTableDataSource } from './verguss-data-table-datasource';
import { DataTableEvent } from './data-table-event';

@Component({
  selector: 'verguss-data-table',
  templateUrl: './verguss-data-table.component.html',
  styleUrls: [ './verguss-data-table.component.css' ]
})
export class VergussDataTableComponent implements OnInit, OnChanges {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @Input() columnList: any;
  @Input() inputDataSource: any;
  @Input() classes: string;
  externalDataSource: any = [];
  @Input() isPaginationEnabled: boolean = true;
  dataSource: VergussDataTableDataSource;
  displayedColumns: any = [];
  columnDataMap: any = [];
  selectedRowIndex: number = -1;
  selectedItem: any;
  tableClass: string = '';

  constructor(public dataTableEvent: DataTableEvent) {

  }

  ngOnChanges(changes) {
    this.dataSource = new VergussDataTableDataSource(this.paginator, this.sort, this.columnList.columnMapping);
    if (this.inputDataSource) {
      this.dataSource.data = this.inputDataSource;
    } else {
      this.dataSource.data = this.externalDataSource;
    }
    if (this.classes) {
      this.setTableClass(this.classes);
    }
  }

  ngOnInit() {
    this.setInputColumns();
    this.dataSource = new VergussDataTableDataSource(this.paginator, this.sort, this.columnList ? this.columnList.columnMapping : []);
    this.dataSource.data = this.externalDataSource;
  }

  setDataSource(dataSource: any) {
    this.dataSource = new VergussDataTableDataSource(this.paginator, this.sort, this.columnList ? this.columnList.columnMapping : []);
    this.dataSource.data = dataSource;
  }

  handleHeaderRowClick(selectedItem, index) {
    this.selectedRowIndex = index;
    this.selectedItem = selectedItem;
    this.dataTableEvent.rowClickEvent.emit(selectedItem);
  }

  setInputColumns() {
    this.displayedColumns = this.columnList.dispalyedColumns;
    this.columnDataMap = this.columnList.columnMapping;
  }

  setColors(selectedItem): any {
    // override and do what ever you want
  }

  setIcons(selectedItem): any {
    // override and do what ever you want
  }

  actionClick(selectedRow: any, column: any) {
    // override and do what ever you want
  }

  isDisabled(selectedRow: any) {
    // override and do what ever you want
  }

  setDisplayedColums(columns: any) {
    this.displayedColumns = columns;
  }

  setColumnDataMap(columns: any) {
    this.columnDataMap = columns;
  }

  setTableClass(tableClass: string) {
    this.tableClass = tableClass;
  }

  setButtonText(row: any, col: any) {
    // columns[6].buttonText = 'test';
  }
}
