
import { fakeAsync, ComponentFixture, TestBed } from '@angular/core/testing';

import { VergussDataTableComponent } from './verguss-data-table.component';

describe('VergussDataTableComponent', () => {
  let component: VergussDataTableComponent;
  let fixture: ComponentFixture<VergussDataTableComponent>;

  beforeEach(fakeAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ VergussDataTableComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(VergussDataTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should compile', () => {
    expect(component).toBeTruthy();
  });
});
