import { APP_INITIALIZER, NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MainNavComponent } from './main-nav/main-nav.component';
import { RouterModule, Routes } from '@angular/router';
import { TranslateService } from './translate/translate.service';
import { TranslationProviders } from './translate/translations';
import { HttpConnection } from './communication/http-connection';
import { OrderListComponent } from './order-list/order-list.component';
import { ManualScanComponent } from './manual-scan/manual-scan.component';
import { DataTableEvent } from './verguss-data-table/data-table-event';
import { OrderGrid } from './order-list/order-grid';
import { DataCacheHandlerService } from './data-cache/data-cache-handler.service';
import { DataCacheService } from './data-cache/data-cache.service';
import { RouterGurad } from './router-gurad';
import { ManualScanDialogComponent } from './manual-scan/manual-scan-dialog/manual-scan-dialog.component';
import { ManualScanEvents } from './manual-scan/manual-scan-events';
import { LocalServiceModule } from './modules/local-service/local-service.module';
import { MotorsComponent } from './modules/local-service/motors/motors.component';
import { SharedModule } from './modules/shared/shared.module';
import { WebSocketService } from './communication/web-socket.service';
import { RecipesComponent } from './modules/local-service/recipes/recipes.component';
import { GroutingComponent } from './grouting/grouting.component';
import { RecipeDialogComponent } from './grouting/recipe-dialog/recipe-dialog.component';
import { GroutingEvents } from './grouting/grouting-events';
import { HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
import { GestureConfig } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ConnectionConfig } from './connection-config';
import { DataCacheEvents } from './data-cache/data-cache-events';


const routes: Routes = [
  // {path: '', redirectTo: '/orders', pathMatch: 'full'},
  {path: 'orders', component: OrderListComponent, canActivate: [ RouterGurad ]},
  {path: 'orders/:id', component: OrderListComponent, canActivate: [ RouterGurad ]},
  {path: 'orders/orderNumber/:orderNumber', component: OrderListComponent, canActivate: [ RouterGurad ]},
  {path: 'motors', component: MotorsComponent},
  {path: 'manualScan', component: ManualScanComponent, canActivate: [ RouterGurad ]},
  {path: 'recipes', component: RecipesComponent}
];

export function loadConfigService(configService: ConnectionConfig): Function {
  return () => {
    return configService.load();
  };
}

@NgModule({
  declarations: [
    AppComponent,
    MainNavComponent,
    OrderListComponent,
    ManualScanComponent,
    OrderGrid,
    ManualScanDialogComponent,
    GroutingComponent,
    RecipeDialogComponent
  ],
  imports: [
    SharedModule,
    LocalServiceModule,
    BrowserAnimationsModule,
    NgbModule.forRoot(),
    RouterModule.forRoot(routes, {onSameUrlNavigation: 'reload'})
  ],
  providers: [
    {provide: HAMMER_GESTURE_CONFIG, useClass: GestureConfig},
    TranslateService,
    TranslationProviders,
    HttpConnection,
    DataTableEvent,
    ManualScanEvents,
    GroutingEvents,
    DataCacheHandlerService,
    DataCacheService,
    WebSocketService,
    RouterGurad,
    ConnectionConfig,
    DataCacheEvents,
    { provide: APP_INITIALIZER, useFactory: loadConfigService , deps: [ConnectionConfig], multi: true },
  ],
  bootstrap: [ AppComponent ],
  entryComponents: [ ManualScanDialogComponent, RecipeDialogComponent ]
})
export class AppModule {
}
