export const environment = {
  production: true,
  baseUrl: 'http://localhost:3000/',
  assetUrl: 'http://localhost/assets/',
  apiUrl: 'http://192.168.10.195/smartProduction/',
  wsUrl: 'http://localhost:3000/'
};
